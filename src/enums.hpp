#ifndef ENUMS_HPP
#define ENUMS_HPP

#include <ostream>


namespace app
{
    /// exit codes enum
    enum exit_codes
    {
        EX_SUCCESS = EXIT_SUCCESS,  ///< success
        EX_FAILURE = EXIT_FAILURE,  ///< fail
        RW_ERROR,                   ///< read/write error
        LOGICAL_ERROR,              ///< logical error
        EOF_REACHED                 ///< end-of-file reached
    };

    /// parenthesis states enum
    enum class parenthesis_states
    {
        not_found,                  ///< non found state
        balanced,                   ///< balanced state
        open_imbalance,             ///< open parenthesis imbalance
        close_imbalance,            ///< close parenthesis imbalance
        parse_error,                ///< sequence parse error
        unknown                     ///< unknown state
    };


    /**
     * @brief operator <<
     * @param lhs std::ostream& instance
     * @param rhs const parenthesis_states enum
     * @return std::ostream& instance
     */
    std::ostream& operator<<(std::ostream& lhs, const parenthesis_states rhs);
}

#endif
