#include "process.hpp"
#include "likely.hpp"
#include <iostream>
#include <stdexcept>


namespace app
{
    process_t::process_t(int argc, char* argv[])
    {
        if (argc > 1)
        {
            _pos = std::atol(argv[1]);
        }
    }

    process_t::~process_t()
    {
        stop();
    }

    void process_t::start()
    {
        if (!_is_run)
        {
            _is_run = true;

            processing_loop();
        }
    }

    void process_t::stop() noexcept
    {
        _is_run = false;
    }

    void process_t::processing_loop()
    {
        std::string line;

        while (likely(_is_run))
        {
            if (likely(std::cin.good()))
            {
                std::getline(std::cin, line);
                if (likely(line.length() > 0))
                {
                    const uint64_t pos = (_pos > 0) ? _pos : line.length() - 1;
                    _pa.analyze_line(line, pos);

                    std::cout << "state: " << _pa.state()
                              << " at position: " << pos
                              << std::endl
                                 ;
                }
            }
            else
            {
                _is_run = false;

                if (std::cin.eof())
                {
                    _exit_code = exit_codes::EOF_REACHED;
                }
                else if (std::cin.bad())
                {
                    _exit_code = exit_codes::RW_ERROR;
                }
                else
                {
                    _exit_code = exit_codes::EX_FAILURE;
                }
            }
        }
    }
}
