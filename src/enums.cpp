#include "enums.hpp"


namespace app
{
    std::ostream& operator<<(std::ostream& lhs, const parenthesis_states rhs)
    {
        switch (rhs)
        {
            case parenthesis_states::not_found:
                lhs << "not_found";
                break;

            case parenthesis_states::balanced:
                lhs << "balanced";
                break;

            case parenthesis_states::open_imbalance:
                lhs << "open_imbalance";
                break;

            case parenthesis_states::close_imbalance:
                lhs << "close_imbalance";
                break;

            case parenthesis_states::parse_error:
                lhs << "parse_error";
                break;

            case parenthesis_states::unknown:
                lhs << "unknown";
                break;

            default:
                break;
        }

        return lhs;
    }
}
