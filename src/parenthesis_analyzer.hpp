#ifndef PARENTHESIS_ANALYZER_T_HPP
#define PARENTHESIS_ANALYZER_T_HPP

#include "enums.hpp"
#include <string>


namespace app
{
    /**
     * @brief The parenthesis_analyzer_t class
     * Analyzes sequence of characters and finds out its state in terms of closed parenthesis.
     */
    class parenthesis_analyzer_t final
    {
    public:
        parenthesis_analyzer_t() = default;
        ~parenthesis_analyzer_t() = default;

        /**
         * @brief state getter
         * @return state enumeration value
         */
        auto state() const noexcept { return _state; }

        /**
         * @brief analyze_line
         * @param text text sequence for analyze for closed parenthesis
         * @param pos arbitraty point, to which analyzing must be performed (if 0 - to the end of line)
         */
        void analyze_line(const std::string& text, const uint64_t pos);

    private:
        parenthesis_states _state = parenthesis_states::unknown;    ///< parenthesis analyzer last state
    };
}

#endif
