#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <stdlib.h>
#include <string>
#include "enums.hpp"
#include "parenthesis_analyzer.hpp"


namespace app
{
    /** @brief The process_t class
     *  @author Konstantin Ivanov
     *  The process implementation class
     */
    class process_t final
    {
    public:
        /** @brief ctor
         * @param argc[in] number of arguments
         * @param argv[in] list of arguments
         */
        process_t(int argc, char* argv[]);
        ~process_t();

        process_t(const process_t&) = delete;
        process_t(process_t&&) = delete;

        process_t& operator=(const process_t&) = delete;
        process_t& operator=(process_t&&) = delete;

        /**
         * @brief exit_code getter
         * @return exit code enumeration value
         */
        auto exit_code() const noexcept { return _exit_code; }

        void start();                               ////< start the processing loop
        void stop() noexcept;                       ////< stop the processing loop

    private:
        void processing_loop();                     ////< perform processing loop

        bool _is_run = false;                       ///< process running flag
        uint64_t _pos = 0;                          ///< position in sequence for parenthesis analyzer
        int _exit_code = exit_codes::EX_SUCCESS;    ///< process exit code

        parenthesis_analyzer_t _pa;                 ///< parenthesis analyzer object
    };
}

#endif
