#include <stdint.h>
#include <signal.h>
#include <iostream>
#include <memory>
#include "process.hpp"


std::unique_ptr<app::process_t> process = nullptr;

void sig_handler(int32_t signal)
{
    switch (signal)
    {
        case SIGKILL:
        case SIGSTOP:
        case SIGINT:
            std::cout << "Signal handled: " << signal << std::endl;
            process->stop();
            break;

        default:
            std::cout << "Unhandled signal: " << signal << std::endl;
            break;
    }
}


int main(int argc, char* argv[])
{
    signal(SIGKILL, sig_handler);
    signal(SIGSTOP, sig_handler);
    signal(SIGINT, sig_handler);

    process.reset(new app::process_t(argc, argv));
    process->start();

    return process->exit_code();
}

