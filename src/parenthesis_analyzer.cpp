#include "parenthesis_analyzer.hpp"
#include "likely.hpp"
#include <iostream>
#include <iomanip>
#include <stack>
#include <stdexcept>


namespace app
{
    void parenthesis_analyzer_t::analyze_line(const std::string& line, const uint64_t pos)
    {
        if (!line.empty() && pos < line.length())
        {
            bool found = false;
            std::stack<char> stack;

            for (uint64_t i = 0; i <= pos; i++)
            {
                char ch = line[i];

                if (unlikely('(' == ch))
                {
                    found = true;
                    stack.push(ch);
                }
                else if (unlikely(')' == ch))
                {
                    found = true;

                    if (!stack.empty() && '(' == stack.top())
                    {
                        stack.pop();
                    }
                    else
                    {
                        stack.push(ch);
                        break;  // closing parenthesis do not close anything
                    }
                }
            }

            // check if all opened parenthesis were closed
            if (likely(found))
            {
                if (stack.empty())
                {
                    _state = parenthesis_states::balanced;
                }
                else
                {
                    if ('(' == stack.top())
                    {
                        _state = parenthesis_states::open_imbalance;
                    }
                    else if (')' == stack.top())
                    {
                        _state = parenthesis_states::close_imbalance;
                    }
                    else
                    {
                        throw std::logic_error("invalid symbol in stack");
                    }
                }
            }
            else
            {
                _state = parenthesis_states::not_found;
            }
        }
        else
        {
            _state = parenthesis_states::parse_error;
        }
    }
}
